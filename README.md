Ansible Role: Nginx
===================
Install and configure `nginx` and `vhost` on Debian servers.

----------

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	nginx_behind_proxy: false
Allow to use `nginx` as http server behind a proxy or load balancer. Setting this variable to `true`, will enable on each `vhost` a log reporting also the `http_x_forwarded_for` header thus logging  the request sender before the proxy.
If you use PHP as backend you can also add the rule `fastcgi_param REMOTE_ADDR $http_x_forwarded_for` to the `location` related to PHP (see below) in order to pass to fastcgi the ip address who actually generate the request, instead of the proxy ip addr.

	nginx_user: "www-data"
By default, `nginx` is executed by user `www-data`. Be sure to allow `nginx_user` read access to assets and write access to folders where user upload will reside (as well as the log folder).

	nginx_worker_processes: "{{ ansible_processor_vcpus | default(ansible_processor_count) }}"
	nginx_worker_connections: "768"
The first one is responsible for letting our virtual server know many workers to spawn once it has become bound to the proper IP and port(s). It is common practice to run 1 worker process per vcore.
`worker_connections`specify to worker processes how many people can simultaneously be served by `nginx`. The default value is 768; however, considering that every browser usually opens up at least 2 connections/server, this number can half. 

	nginx_multi_accept: "off"
If multi_accept is disabled, a worker process will accept one new connection at a time. Otherwise, a worker process will accept all new connections at a time.

	nginx_sendfile: "on"
	nginx_tcp_nopush: "on"
	nginx_tcp_nodelay: "on"
	nginx_keepalive_timeout: "65"
	nginx_types_hash_max_size: "2048"
	nginx_server_names_hash_bucket_size: "64"
	nginx_log_dir: "/var/log/nginx"
	nginx_access_log: "access.log"
	nginx_error_log: "error.log"
Various `nginx` default directive, you can configure these variables, to tune your deployment.

    nginx_log_rotate_period: "weekly"
    nginx_log_rotate_keep: 52
These variable are used to configure logrotate script for nginx: `period` says when to rotate and `keep` stand for the number of rotated file the server must keep before deleting


This role will add a list of header to the server response, in particular:

	add_header Strict-Transport-Security "max-age=31536000";
	add_header X-Frame-Options DENY;
	add_header X-Content-Type-Options nosniff;
	add_header X-Xss-Protection "1; mode=block";
	add_header Referrer-Policy strict-origin-when-cross-origin;
You can control the referrer policy by overriding `nginx_header_referrer_policy` variable (`strict-origin-when-cross-origin` by default)


	nginx_remove_default_vhost: true
By default this role will remove the default vhost configured within the installation of `nginx`, you can override this behavior simply setting this variable to `false`.
If you want to name your vhost `default` you **need** to disable this behavior. 

	nginx_vhosts: []
The list of all the vhost that will be configured in the `nginx` scope. Each vhost has these variable:

	id: 'exemple.com'  ## default n/a
	ssl_redirect: [true | false]  # default n/a
	ssl_redirect_listen:
		- '80 default_server'
	listen: 
        - '443 ssl default_server'
	server_name: 'example.com'  # default: n/a
	server_aliases: 'www.example.com'  # default: ""
	root: '/var/www/example.com'  # default: n/a
	public: "html" # default n/a
	user:  # default n/a: owner of the project
	index: 'index.html index.htm' 
	
	# Properties that are only added if defined:
	ssl_certificate: '/path/to/cert'
	ssl_certificate_key: '/path/to/certkey' 
	error_page: 
        - 404 = /404.php
    log_dir: '/vhost/path/to/log'
    access_log: 'access.log'
	error_log: 'error.log'
	extras:
	    - 'if ($request_uri ~* "^(.*\/)index\.php(\/|$)(.*$)") {',
	    - '   return 301 $1$3;',
        - '}'
    locations: 
	    - declare: ~ \.php$
		  rules:
		      - include fastcgi_params
		      - try_files /index.php =404
			  - fastcgi_split_path_info ^(.+\.php)(/.+)$
              - fastcgi_pass unix:/var/run/php5-fpm.sock
              - fastcgi_index index.php
              - fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name
              - fastcgi_param REMOTE_ADDR $http_x_forwarded_for  # as described above the IP before proxy
              
Only one of our server blocks can have the `default_server` specification. 
This specifies which block should server a request if the server_name requested does not match any of the available server blocks.
If `ssl_redirect` is set to `true` you need to specify in the `ssl_redirect_listen` the `ip:port` where the server is listening (default `80 default_server`) , and only the ssl `ip:port`  in the variable `listen`. It will also include a redirect to `https` rule automatically.
Otherwise if `ssl_redirect` is set to `false`, you can use the `listen` variable ignoring `ssl_listen`.

Variable `root` and `public` is used to determine where the assets (`{{root}}/{{public}}`) are and `user` is the owner of that directory.
By default the index of the website are `index.html and index.htm` but you can override , e.g. if you are using PHP you should use `index.php`

In order to works as expected the paths linked by `ssl_certificate` and `ssl_certificate_key` must exist and contain the actual certificate chain and key for the server.

You can define extra rules in the `extras` variable: a list of line, each one added to the vhost config as is. E.g the extras above will result in:

	if ($request_uri ~* "^(.*\/)index\.php(\/|$)(.*$)") {
		return 301 $1$3;',
    }
used to remove from request the `index.php` to show a more clean URL.

The `locations` is a list of `nginx` location. First of all you use a `declare` statement defining what the location should match: for example `~ \.php$`. After that u declare which are the `rules` if the pattern is matched. The above declaration translates to the vhost config in this way:

	location ~ \.php$ {
        include fastcgi_params;
        try_files /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param REMOTE_ADDR $http_x_forwarded_for;
    }
   
Dependencies
-------------------
None.

Example Playbook
--------------------------
    - name: install and configure webservers
      hosts: webservers
      role:
        - role: nginx
          nginx_behind_proxy: true
          nginx_user: www-data
          nginx_vhosts: 
            - id: www.example.com
              listen:
                - 10.0.0.2:443 ssl default_server
              ssl_redirect: true
              ssl_redirect_listen: 
                - 10.0.0.2:80 default_server
              ssl_certificate: /etc/ssl/example.cert
              ssl_certificate_key: /etc/ssl/example.key
              server_name: example.com
              server_aliases: 'www.example.com'
              root: /var/www/example.com
              public: html
              user: administrator
              index: index.php
              log_dir: '{{ project.project_root }}/log'
              access_log: access.log
              error_log: error.log
              error_page:
                - '403 = /403.php'
                - '500 = /500.php'
              extras:
                - 'if ($request_uri ~* "^(.*\/)index\.php(\/|$)(.*$)") {'
                - '   return 301 $1$3;'
                - '}'
	             
              locations:
                - declare: '/'
                  rules:
                      - 'try_files $uri $uri/ /index.php$is_args$args'
                - declare: '~ \.php$'
                  rules:
                      - 'include fastcgi_params'
                      - 'try_files /index.php =404'
                      - 'fastcgi_split_path_info ^(.+\.php)(/.+)$'
                      - 'fastcgi_pass unix:/var/run/example.com.sock'
                      - 'fastcgi_index index.php'
                      - 'fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name'
                      - 'fastcgi_param REMOTE_ADDR $http_x_forwarded_for'

